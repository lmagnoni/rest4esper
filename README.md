# Esper + REST + JMS
This project augment Esper with a simple REST API for browsing in-memory events and patterns and with a custom JMS/STOMP source to inject messages from brokers as events. 

Project based on [Spring](http://projects.spring.io/spring-framework/) and [Spring MVC](http://docs.spring.io/spring/docs/current/spring-framework-reference/html/mvc.html) for the REST API, and a bit of JSP/JSTL. The [STOMP/JMS binding from Fusesource](https://github.com/fusesource/stompjms) is used to read from STOMP brokers as standard JMS source, and [Spring Integration](http://projects.spring.io/spring-integration/) takes care of connections handling. Broker connection may require authentication, both username/password and certificate are supported. The project comes with binding for the [FTS](http://information-technology.web.cern.ch/services/file-transfer) transfer messages in JSON.

Clone, configure broker settings on *servlet-context.xml* and run:

```
mvn jetty:run
```

the REST API will be available on:

```
http://localhost/cep/pattern
```