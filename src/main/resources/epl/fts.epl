// Declare the name of this module
module cep.fts;

import ch.cern.cep.*;
import ch.cern.cep.esper.*;

// Bind event schema to the corresponding Metis classes
create schema FTSMessage as ch.cern.cep.FTSMessage;
create schema PatternMatch as ch.cern.cep.DuplicatedEvent;

//create window PatternAWindow.win:keepall() as PatternMatch;
create window PatternAWindow.win:time(48 hour) as PatternMatch;
insert into PatternAWindow select * from PatternMatch(type is 'A');

//create window PatternBWindow.win:keepall() as PatternMatch;
create window PatternBWindow.win:time(48 hour) as PatternMatch;
insert into PatternBWindow select * from PatternMatch(type is 'B');

//create window PatternCWindow.win:keepall() as PatternMatch;
create window PatternCWindow.win:time(48 hour) as PatternMatch;
insert into PatternCWindow select * from PatternMatch(type is 'C');

//Careful, this should be as bis as the bigger pattern interval
create window LastFTSMessages.win:time(1 hour) as FTSMessage;
insert into LastFTSMessages select * from FTSMessage();

create window DupMessages.win:time(48 hour) as FTSMessage;
insert into DupMessages select m.* from LastFTSMessages as m, PatternMatch as p unidirectional where m.cep_msg_uuid is p.uuid1;
insert into DupMessages select m.* from LastFTSMessages as m, PatternMatch as p unidirectional where m.cep_msg_uuid is p.uuid2;


create context CtxEach5Minutes
initiated @now and pattern [every timer:interval(5 min)]
terminated after 5 minute;

@Name('Print_Message_Count')
@Print(severity = Print$Level.INFO)
context CtxEach5Minutes
select endpnt,  count(*) from FTSMessage group by endpnt
output snapshot when terminated;

//@Print(severity = Print$Level.INFO)
//select * from LastFTSMessages;

//@Name('Detect_Duplicated_Transfers')
//@Print(severity = Print$Level.INFO)
//select a,b
//from pattern [every a=FTSMessage -> 
//b=FTSMessage(src_url=a.src_url, dst_url=a.dst_url) where timer:within(10 min)];


//Pattern A 
//"Detects 2 subsequent FTS messages with the same 'src_url' and 'dst_url' for the same 'endpnt' in a 10 minutes window.");
insert into PatternMatch
select 'A' as type, current_timestamp() as timestamp, a.cep_msg_uuid as uuid1, b.cep_msg_uuid as uuid2, a.tr_id as tr_id
from pattern [every a=FTSMessage -> 
b=FTSMessage(src_url=a.src_url, dst_url=a.dst_url, endpnt=a.endpnt) where timer:within(10 min)];

//Pattern B
//"Detects 2 subsequent FTS messages with the same 'file_id', 'retry' count, and  the same 'endpnt' in a 10 minutes window.");
insert into PatternMatch
select 'B' as type, current_timestamp() as timestamp, a.cep_msg_uuid as uuid1, b.cep_msg_uuid as uuid2, a.tr_id as tr_id
from pattern [every a=FTSMessage -> 
b=FTSMessage(file_id=a.file_id, endpnt=a.endpnt, retry=a.retry) where timer:within(10 min)];

//Pattern B
//"Detects 2 subsequent FTS messages with the same 'dst_url' for different 'endpnt' in an overlapping transfer time interval in a 10 minutes window.");
insert into PatternMatch
select 'C' as type, current_timestamp() as timestamp, a.cep_msg_uuid as uuid1, b.cep_msg_uuid as uuid2, a.tr_id as tr_id
from pattern [every a=FTSMessage -> 
b=FTSMessage(dst_url=a.dst_url, endpnt != a.endpnt, tr_timestamp_start <= a.tr_timestamp_complete) where timer:within(10 min)];




@Name('Print PatternA')
@Print(severity = Print$Level.INFO)
select * from PatternAWindow;

@Name('Print PatternB')
@Print(severity = Print$Level.INFO)
select * from PatternBWindow;

@Name('Print PatternC')
@Print(severity = Print$Level.INFO)
select * from PatternCWindow;
