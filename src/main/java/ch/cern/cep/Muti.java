package ch.cern.cep;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Lazy;
import org.springframework.jms.JmsException;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

import ch.cern.cep.esper.CEPService;

@Lazy(value=false)
public class Muti {
    
    private static final Log logger = LogFactory.getLog(Muti.class);
    
    @Autowired
    private ApplicationContext context;
    
    /**
     * Standar JMS containers.
     */
    private Map<String, DefaultMessageListenerContainer> jmsListeners;
    
    @Autowired
    private CEPService cep;

    public Muti() {
        logger.info("Initiailizing orchestration...");
        
        /**
         * Registering a new JVM shutdown hook for this thread.
         * The @ShutdownHook thread contains the clean up code
         * for connections and subscriptions.
         */
        ShutdownHook shutdownHook = new ShutdownHook(this);
        Runtime.getRuntime().addShutdownHook(shutdownHook);
    }
    
    
    @PostConstruct
    public void start() {
        logger.info("Starting up");
        
        jmsListeners = context.getBeansOfType(DefaultMessageListenerContainer.class);
        logger.info(jmsListeners.size()+" JMSListener created and ready to be started.");
        
        
        cep.configure();
        
        cep.start();
        
        for (Map.Entry<String, DefaultMessageListenerContainer> entry: jmsListeners.entrySet()) {
            try {
                entry.getValue().start();
            } catch (JmsException e) {
                logger.error("Problem starting jms message listerner container "
                             + entry.getKey(), e);
            }
        }
        
    }
    
    @PreDestroy
    public void shutdown() {
        logger.info("Shutting down orchestrator");
        
        for (Map.Entry<String, DefaultMessageListenerContainer> entry: jmsListeners.entrySet()) {
            try {
                entry.getValue().stop();
            } catch (JmsException e) {
                logger.error("Problem stopping jms message listerner container "
                             + entry.getKey(), e);
            }
        }
        
        cep.stop();
        
    }
    
    /**
     * A shutdown hook is simply an initialized but
     * non-started thread that will be started when the JVM begins
     * its shutdown sequence.
     */
    private class ShutdownHook extends Thread {
        private final Muti riccardo;
        public ShutdownHook(final Muti r) {
            this.riccardo = r;
        }
        public void run() {
            riccardo.shutdown();
        }
    }

    
    public void setCep(CEPService cep) {
        this.cep = cep;
    }
    
    //@Override
    public void setApplicationContext(ApplicationContext applicationContext)  {
        this.context = applicationContext;
    }
}
