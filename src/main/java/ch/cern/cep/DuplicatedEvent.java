package ch.cern.cep;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DuplicatedEvent {
    
    private String type;
    private Long timestamp;
    private String date;
    private String uuid1;
    private String uuid2;
    private String tr_id;
    
    
    public Long getTimestamp() {
        return timestamp;
    }
    public String getDate() {
        return date;
    }
    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
        String FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
        Locale TIMEZONE = new Locale("CH");
        SimpleDateFormat dateFormat = new SimpleDateFormat(FORMAT, TIMEZONE);
        this.date = dateFormat.format(new Date(timestamp));
    }
    public String getUuid1() {
        return uuid1;
    }
    public void setUuid1(String uuid1) {
        this.uuid1 = uuid1;
    }
    public String getUuid2() {
        return uuid2;
    }
    public void setUuid2(String uuid2) {
        this.uuid2 = uuid2;
    }
    public String getTr_id() {
        return tr_id;
    }
    public void setTr_id(String tr_id) {
        this.tr_id = tr_id;
    }
    
    public void setType(String t) {
        this.type = t;
    }
    public String getType() {
        return type;
    }

}
