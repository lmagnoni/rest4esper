package ch.cern.cep.jms;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonStructure;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.fusesource.stomp.jms.message.StompJmsBytesMessage;
import org.fusesource.stomp.jms.message.StompJmsTextMessage;
import org.springframework.beans.factory.annotation.Autowired;

import ch.cern.cep.FTSMessage;
import ch.cern.cep.esper.CEPService;

public class Listener implements MessageListener{

    private static final Log logger = LogFactory.getLog(Listener.class);

    private CEPService cep;

    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }
    

    public Listener(CEPService cep) {
        super();
        this.cep = cep;
    }




    @Override
    public void onMessage(Message message) {
        if (message instanceof StompJmsBytesMessage) {
            if (message instanceof StompJmsBytesMessage) {
                try {
                    StompJmsBytesMessage m = (StompJmsBytesMessage) message;
                    //logger.debug("message ByteMessage "+m.getContent().ascii().toString());
                    
                    JsonReader jr = Json.createReader(new StringReader(m.getContent().ascii().toString()));
                    JsonObject json = jr.readObject();
                    jr.close();
                    
                    cep.injectEvent(FTSMessage.fromJson(json.toString()));
                    
                } catch (Exception e) {
                    logger.error("Something wrong appened",e);
                }

            } else if ((message instanceof StompJmsTextMessage)) {
                logger.error("message TextMessage received");
                //events = adapter.decode((StompJmsBytesMessage) message);
            } else {
                logger.error("Unknown message type received: "
                        + message.getClass().getName());
                return;
            }

        } else {
            logger.info((message).getClass());
            throw new IllegalArgumentException("Message must be of type Stomp");
        }

    }

}
