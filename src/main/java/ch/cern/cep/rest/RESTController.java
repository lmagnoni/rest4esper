package ch.cern.cep.rest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ch.cern.cep.DuplicatedEvent;
import ch.cern.cep.FTSMessage;
import ch.cern.cep.esper.EsperDAO;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping(value="/cep")
public class RESTController {
	
	private static final Logger logger = LoggerFactory.getLogger(RESTController.class);
	
	@Autowired
	private EsperDAO cepDAO;
	
	@RequestMapping(value = "/fts/message/{uuid}", method = RequestMethod.GET)
    public String getMessage(@PathVariable("uuid") String uuid, Model model) {
        logger.info("Retrieving FTSMessage per ID");
        FTSMessage message = cepDAO.getFTSMessageByUUID(uuid);
        model.addAttribute("message", message);
        return "message";
    }
	
	
	@RequestMapping(value = "/pattern", method = RequestMethod.GET)
    public String getPatterns(Model model) {
        logger.info("Get patterns description");
        List<Map<String, String>> patterns = new LinkedList();
        
        Map<String, String> patternA = new HashMap();
        patternA.put("ID","a");
        patternA.put("description", "Detects 2 subsequent FTS messages with the same 'src_url' and 'dst_url' for the same 'endpnt' in a 10 minutes window");
        patternA.put("count", Integer.toString(cepDAO.getPatternAEvents().size()));
        patterns.add(patternA);
        
        Map<String, String> patternB = new HashMap();
        patternB.put("ID","b");
        patternB.put("description", "Detects 2 subsequent FTS messages with the same 'file_id', 'retry' count and the same 'endpnt' in a 10 minutes window.");
        patternB.put("count", Integer.toString(cepDAO.getPatternBEvents().size()));
        patterns.add(patternB);
        
        Map<String, String> patternC = new HashMap();
        patternC.put("ID","c");
        patternC.put("description", "Detects 2 subsequent FTS messages with the same 'dst_url' for different 'endpnt' in an overlapping transfer time interval in a 10 minutes window.");
        patternC.put("count", Integer.toString(cepDAO.getPatternCEvents().size()));
        patterns.add(patternC);
        
        
        model.addAttribute("events", patterns);

        return "patterns";
    }
	
	@RequestMapping(value = "/pattern/a", method = RequestMethod.GET)
    public String getPatternA(Model model) {
        logger.info("Retrieving PatternA Events");
        
        List<DuplicatedEvent> res = cepDAO.getPatternAEvents();
        model.addAttribute("events", res);

        return "patterna";
    }
	
	@RequestMapping(value = "/pattern/b", method = RequestMethod.GET)
    public String getPatternB(Model model) {
        logger.info("Retrieving PatternB Events");
        
        List<DuplicatedEvent> res = cepDAO.getPatternBEvents();
        model.addAttribute("events", res);

        return "patternb";
    }
	
	@RequestMapping(value = "/pattern/c", method = RequestMethod.GET)
    public String getPatternC(Model model) {
        logger.info("Retrieving PatternC Events");
        
        List<DuplicatedEvent> res = cepDAO.getPatternCEvents();
        model.addAttribute("events", res);

        return "patternb";
    }
	
	
	
}
