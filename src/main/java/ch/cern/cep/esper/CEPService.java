package ch.cern.cep.esper;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.LinkedList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.espertech.esper.client.Configuration;
import com.espertech.esper.client.EPException;
import com.espertech.esper.client.EPOnDemandQueryResult;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;
import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.UpdateListener;
import com.espertech.esper.client.deploy.DeploymentOptions;
import com.espertech.esper.client.deploy.DeploymentResult;
import com.espertech.esper.client.deploy.EPDeploymentAdmin;
import com.espertech.esper.client.deploy.Module;

/**
 * Facade to mask interactions with the CEP engine from exposing implementation
 * details.
 *
 * @author lucamag
 *
 */

public class CEPService {
    // class logger
    private static final Log logger = LogFactory.getLog(CEPService.class);

    @Autowired
    private ApplicationContext ctx;

    private EPServiceProvider esper;

    private final String name;

    // Using SpEL to get properties list of files
    @Value("#{configuration['epl.files'].split(',')}")
    private List<String> modules;

    @Value("#{configuration['epl.directory']}")
    private String eplDir;

    @Autowired
    @Qualifier("configuration")
    private Properties metisProperties;

    /**
     * When injectors receive a callback, they create and add a task into the
     * non blocking pool thread queue. A thread from the pool execute the task
     * and it will deal with the insertion of the event in Esper. IMPORTANT:
     * Esper by default use the same application-thread which invokes any of the
     * {@code sendEvent} methods to process the event fully and also deliver
     * output events to listeners and subscribers. This can be tuned using the
     * inbound and outbound threading options.
     */

    private ExecutorService injectorCallbackPool;
    private ScheduledExecutorService scheduledExecutorService;

    private Map<String, Long> lastDeployed;

    /**
     * Ctor.
     *
     * @param name
     *            the name of this instance of the cep engine
     * @param enableMetrics
     *            flag to enable the ESPER built-in metrics reporting
     */

    public CEPService(final String name, final Boolean useInternalTimer,
            final Boolean enableMetrics) {
        this(name, useInternalTimer, enableMetrics, 1, 0, 1);
    }

    public CEPService(final String name, final Boolean useInternalTimer,
            final Boolean enableMetrics, final Integer inputPool,
            final Integer outPool, final Integer schedPool) {

        Configuration esperConfig = new Configuration();
        // esperConfig.configure(new java.io.File(esperConfigurationFile));
        esperConfig.addImport("ch.cern.cep.esper.*");

        // Configure mathematical context to allow BigDecimal math operation
        esperConfig.getEngineDefaults().getExpression()
                .setMathContext(new MathContext(10, RoundingMode.CEILING));

        // In case of internal timer disabled, time events have to be injected
        // by external threads,
        // no internal timer thread exists then.
        esperConfig.getEngineDefaults().getThreading()
                .setInternalTimerEnabled(useInternalTimer);
        esperConfig.getEngineDefaults().getThreading()
                .setThreadPoolTimerExec(true);
        esperConfig.getEngineDefaults().getThreading()
                .setThreadPoolTimerExecNumThreads(10);
        esperConfig.getEngineDefaults().getThreading()
                .setInternalTimerMsecResolution(10);
        if (outPool > 0) {
            esperConfig.getEngineDefaults().getThreading()
                    .setThreadPoolOutbound(true);
            esperConfig.getEngineDefaults().getThreading()
                    .setThreadPoolOutboundNumThreads(outPool);
        }
        // JMX reporting
        esperConfig.getEngineDefaults().getMetricsReporting()
                .setJmxEngineMetrics(true);

        // Enable statemetn report
        // esperConfig.getEngineDefaults().getMetricsReporting().setEnableMetricsReporting(true);
        // esperConfig.getEngineDefaults().getMetricsReporting().setEngineInterval(1000);

        // Enable Esper metrics
        if (enableMetrics) {
            esperConfig.getEngineDefaults().getMetricsReporting()
                    .setEnableMetricsReporting(true);
            esperConfig.getEngineDefaults().getMetricsReporting()
                    .setEngineInterval(1000);
            esperConfig.getEngineDefaults().getMetricsReporting()
                    .setStatementInterval(1000);
            esperConfig.getEngineDefaults().getMetricsReporting()
                    .setThreading(true);
        }

        // Enable priority
        esperConfig.getEngineDefaults().getExecution().setPrioritized(true);
        logger.info("Creating Metis Inbound pool with " + inputPool
                + " threads.");
        injectorCallbackPool = Executors.newFixedThreadPool(inputPool);
        logger.info("Creating Metis Scheduler pool with " + schedPool
                + " threads.");
        scheduledExecutorService = Executors.newScheduledThreadPool(schedPool);

        logger.debug("Priority "
                + esperConfig.getEngineDefaults().getExecution()
                        .isPrioritized());

        // esperConfig.getEngineDefaults().getTimeSource().setTimeSourceType(
        // TimeSourceType.NANO);

        esper = EPServiceProviderManager.getProvider(name, esperConfig);

        this.name = name;

    }

    public EPServiceProvider getEsper() {
        return esper;
    }

    /**
     * This method prepares the CEP engine to receive events from injectors.
     * <p>
     * Statements must be created and deployed and listener/subscriber in place.
     * 
     * @throws ConfigurationException
     */
    public void configure() {
        logger.debug("Configuring CEP engine...");
        this.deployModules();
        logger.info("CEP engine configured");
    }

    private void deployModules() {
        logger.debug("Loading all modules...");
        for (String moduleName : modules) {
            this.deployModule(moduleName);
        }
    }

    public void initAndConfigure() {
        esper.initialize();
        this.configure();
    }

    public void registerStatement(final String statement, final String name)
            throws Exception {
        EPStatement stmt = esper.getEPAdministrator()
                .createEPL(statement, name);
        this.processStatementAnnotations(stmt);
    }

    public void injectEventSynch(final Object obj) {
        try {
            esper.getEPRuntime().sendEvent(obj);
        } catch (EPException e) {
            logger.error("Problem in event injection: ", e);
        }
    }

    public void injectEvent(final Object event) {

        // Deferred send event
        Runnable task = new Runnable() {

            @Override
            public void run() {
                try {
                    esper.getEPRuntime().sendEvent(event);
                } catch (EPException e) {
                    logger.error("Problem in event injection: ", e);
                }
            }
        };
        try {
            injectorCallbackPool.execute(task);
        } catch (RejectedExecutionException e) {
            logger.error("Error submitting task: ", e);
        } catch (EPException e) {
            logger.error("Very bad! Unexpected exception thrown ", e);
        }

    }

    private File modfile(final String moduleName) throws IOException {
        String modulePath = eplDir != null ? eplDir + "/" + moduleName
                : moduleName;
        Resource moduleResource = new ClassPathResource(modulePath);
        return moduleResource.getFile();
    }

    /**
     * Read, deploy and process annotation for the specified module. An
     * exception will be thrown if the specified module is already in use.
     * 
     * @param moduleName
     * @throws CEPConfigurationError
     */
    public void deployModule(final String moduleName) {
        logger.info("Deploying module: " + moduleName);
        try {

            File moduleFile = modfile(moduleName);
            EPDeploymentAdmin deployAdmin = esper.getEPAdministrator()
                    .getDeploymentAdmin();
            Module module = deployAdmin.read(moduleFile);
            DeploymentResult deployResult = deployAdmin.deploy(module,
                    this.getModuleOptions(), moduleName);
            List<EPStatement> statements = deployResult.getStatements();
            for (EPStatement statement : statements) {
                processStatementAnnotations(statement);
            }
            logger.debug("Module " + moduleName + " loaded");

        } catch (Exception e) {
            logger.error("Problem deplying module", e);
            // throw new
            // MetisException.CEPConfigurationError("Problem processing EPL modules",
            // e);
        }

    }

    /**
     * Process @Subscriber and @Listeners custom annotations.
     *
     * @param statement
     * @throws Exception
     */
    private void processStatementAnnotations(final EPStatement statement)
            throws Exception {

        Annotation[] annotations = statement.getAnnotations();
        for (Annotation annotation : annotations) {

            if (annotation instanceof Print) {
                Print p = (Print) annotation;
                ch.cern.cep.esper.PrintListener listener = new ch.cern.cep.esper.PrintListener(
                        p.severity(), statement.getName());
                statement.addListener((UpdateListener) listener);

                logger.debug("Listener "
                        + listener.getClass().getCanonicalName()
                        + " attached to statement " + statement.getName());

            }
        }
    }

    /**
     * Destroy Esper context, cleaning the working memory from all events, and
     * recreate the statements as from configuration.
     * 
     * @throws CEPConfigurationError
     */
    public void reset() {
        logger.debug("CEP engine resetting...");
        esper.destroy();
        esper.initialize();
        // this.configure();
    }

    /**
     * Get the subscriber instance from the Spring context configuration,
     * allowing for multiple implementation for production and testing.
     *
     * @param fqdn
     * @throws Exception
     */
    private Object getBeanWithSpring(final String fqdn) throws Exception {
        return ctx.getBean(Class.forName(fqdn));
    }

    private DeploymentOptions getModuleOptions() {

        DeploymentOptions options = new DeploymentOptions();
        // options.setIsolatedServiceProvider("validation"); // we isolate any
        // statements
        // options.setValidateOnly(true); // validate leaving no started
        // statements
        // options.setFailFast(false); // do not fail on first error

        return options;
    }

    public EPOnDemandQueryResult executeQuery(final String epl) {
        return esper.getEPRuntime().executeQuery(epl);
    }

    public void start() {
        logger.debug("Starting Esper...");

    }

    public void stop() {
        logger.debug("Shutting down callback thread pool...");
        injectorCallbackPool.shutdown();
        logger.debug("Shutting down scheduled executor service...");
        scheduledExecutorService.shutdown();
        try {
            scheduledExecutorService.awaitTermination(2, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            logger.error("CEP scheduled executor service taking too long...", e);
        }
        logger.debug("Shutting down Esper...");
        esper.destroy();
    }

    public String getName() {
        return this.name;
    }

}
