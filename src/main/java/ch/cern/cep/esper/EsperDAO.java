package ch.cern.cep.esper;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.espertech.esper.client.EPOnDemandQueryResult;
import com.espertech.esper.client.EventBean;

import ch.cern.cep.DuplicatedEvent;
import ch.cern.cep.FTSMessage;

public class EsperDAO {
    
    private static final Logger logger = LoggerFactory.getLogger(EsperDAO.class);
    
    @Autowired
    private CEPService cep;
    
    public FTSMessage getFTSMessageByUUID(String uuid) {

        EPOnDemandQueryResult result = cep.executeQuery("select * from DupMessages where cep_msg_uuid='"+uuid+"'");
        if (result.getArray().length > 0) {
            Object obj = result.getArray()[0].getUnderlying();
            logger.trace("Found: "+(FTSMessage)obj);
            return (FTSMessage)obj;
        } else {
            logger.error("No message found with such UUID "+uuid);
            return null;
        }
        
    }
    
    public List<DuplicatedEvent> getPatternAEvents() {
        EPOnDemandQueryResult result = cep.executeQuery("select * from PatternAWindow order by timestamp desc");
        List<DuplicatedEvent> list = new LinkedList();
        for(EventBean e:result.getArray()) {
            DuplicatedEvent dup = (DuplicatedEvent)e.getUnderlying();
            logger.trace(dup.getTimestamp()+", "+dup.getUuid1());
            list.add(dup);
        }
        return list;
    }
    
    public List<DuplicatedEvent> getPatternBEvents() {
        EPOnDemandQueryResult result = cep.executeQuery("select * from PatternBWindow order by timestamp desc");
        List<DuplicatedEvent> list = new LinkedList();
        for(EventBean e:result.getArray()) {
            DuplicatedEvent dup = (DuplicatedEvent)e.getUnderlying();
            logger.trace(dup.getTimestamp()+", "+dup.getUuid1());
            list.add(dup);
        }
        return list;
    }
    
    public List<DuplicatedEvent> getPatternCEvents() {
        EPOnDemandQueryResult result = cep.executeQuery("select * from PatternCWindow order by timestamp desc");
        List<DuplicatedEvent> list = new LinkedList();
        for(EventBean e:result.getArray()) {
            DuplicatedEvent dup = (DuplicatedEvent)e.getUnderlying();
            logger.trace(dup.getTimestamp()+", "+dup.getUuid1());
            list.add(dup);
        }
        return list;
    }
    

    public CEPService getCep() {
        return cep;
    }

    public void setCep(CEPService cep) {
        this.cep = cep;
    }

    
}
