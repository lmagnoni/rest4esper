package ch.cern.cep.esper;


/**
 * Custom annotation to print the result of EPL statements.
 */
public @interface Print {

    public enum Level {
        TRACE, DEBUG, INFO, WARNING, ERROR;
    }

    Level severity() default Level.INFO;

}
