package ch.cern.cep;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import javax.json.JsonObject;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

/**
{"tr_id":"2015-07-15-1028__cmssrm-kit.gridka.de__bsrm-3.t2.ucsd.edu__12077584__681e69d6-dd43-4b7b-aff9-8a2177d430c6",
"endpnt":"cmsfts3.fnal.gov",
"src_srm_v":"2.2.0",
"dest_srm_v":"2.2.0",
"vo":"cms",
"src_url":"srm://cmssrm-kit.gridka.de:8443/srm/managerv2?SFN=/pnfs/gridka.de/cms/store/PhEDEx_LoadTest07/LoadTest07_DE_KIT_77",
"dst_url":"srm://bsrm-3.t2.ucsd.edu:8443/srm/v2/server?SFN=/hadoop/cms/phedex/store/PhEDEx_LoadTest07/LoadTest07_Debug_DE_KIT/US_UCSD/484/LoadTest07_KIT_77_xMOMKBVXW2eGmlk7_484",
"src_hostname":"cmssrm-kit.gridka.de",
"dst_hostname":"bsrm-3.t2.ucsd.edu",
"src_site_name":"",
"dst_site_name":"",
"t_channel":"srm://cmssrm-kit.gridka.de__srm://bsrm-3.t2.ucsd.edu",
"timestamp_tr_st":"1436956122635",
"timestamp_tr_comp":"1436956354314",
"timestamp_chk_src_st":"",
"timestamp_chk_src_ended":"",
"timestamp_checksum_dest_st":"",
"timestamp_checksum_dest_ended":"",
"t_timeout":"8720",
"chk_timeout":"1800",
"t_error_code":"",
"tr_error_scope":"",
"t_failure_phase":"",
"tr_error_category":"",
"t_final_transfer_state":"Ok",
"tr_bt_transfered":"2684354560",
"nstreams":"12",
"buf_size":"0",
"tcp_buf_size":"0",
"block_size":"0",
"f_size":"2684354560",
"time_srm_prep_st":"1436956120742",
"time_srm_prep_end":"1436956122573",
"time_srm_fin_st":"1436956354314",
"time_srm_fin_end":"1436956354858",
"srm_space_token_src":"",
"srm_space_token_dst":"",
"t__error_message":"",
"tr_timestamp_start":"1436956116000.000000",
"tr_timestamp_complete":"1436956356000.000000",
"channel_type":"urlcopy",
"user_dn":"/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=jletts/CN=409293/CN=James Letts",
"file_metadata":"",
"job_metadata":"",
"retry":"0",
"retry_max":"0",
"job_m_replica":"false",
"job_state":"UNKNOWN"}
 */


/**
 * DEBUG: ch.cern.cep.FTSMessage - JSON: {"tr_id":"2015-07-16-1106__srm.triumf.ca__charon01.westgrid.ca__403431971__a9839a3a-2baa-11e5-a91f-02163e006d0a","endpnt":"fts3.cern.ch","src_srm_v":"2.2.0","dest_srm_v":"2.2.0","vo":"atlas","src_url":"srm://srm.triumf.ca:8443/srm/managerv2?SFN=/atlas/atlasdatadisk/rucio/tests/0a/af/ESD.db97383759194f5f97fc97c58bffd319","dst_url":"srm://charon01.westgrid.ca:8443/srm/managerv2?SFN=/pnfs/westgrid.uvic.ca/data/atlas/atlasdatadisk/rucio/tests/0a/af/ESD.db97383759194f5f97fc97c58bffd319","src_hostname":"srm.triumf.ca","dst_hostname":"charon01.westgrid.ca","src_site_name":"","dst_site_name":"","t_channel":"srm://srm.triumf.ca__srm://charon01.westgrid.ca","timestamp_tr_st":"1437044773860","timestamp_tr_comp":"1437044779788","timestamp_chk_src_st":"1437044771666","timestamp_chk_src_ended":"1437044772370","timestamp_checksum_dest_st":"1437044779979","timestamp_checksum_dest_ended":"1437044780161","t_timeout":"902","chk_timeout":"1800","t_error_code":"","tr_error_scope":"","t_failure_phase":"","tr_error_category":"","t_final_transfer_state":"Ok","tr_bt_transfered":"1048576","nstreams":"1","buf_size":"0","tcp_buf_size":"0","block_size":"0","f_size":"1048576","time_srm_prep_st":"1437044771666","time_srm_prep_end":"1437044773860","time_srm_fin_st":"1437044779789","time_srm_fin_end":"1437044779979","srm_space_token_src":"","srm_space_token_dst":"ATLASDATADISK","t__error_message":"","tr_timestamp_start":"1437044770000.000000","tr_timestamp_complete":"1437044781000.000000","channel_type":"urlcopy","user_dn":"/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=ddmadmin/CN=531497/CN=Robot: ATLAS Data Management",
 * "file_metadata":{"adler32":"0b85a94d","name":"ESD.db97383759194f5f97fc97c58bffd319","src_rse":"IN2P3-CC_DATADISK","request_id":"3ce5e539181d49ffa8a7011c33ff0438","filesize":1048576,
 * "dest_rse_id":"df7d9b15e89240229b860ebe0477c570","scope":"tests","dst_rse":"CA-VICTORIA-WESTGRID-T2_DATADISK","activity":"Functional Test","md5":null},
 * "job_metadata":{"multi_sources":true,"issuer":"rucio"},"retry":"0","retry_max":"0","job_m_replica":"true","job_state":"FINISHED"}

 * @author lucamag
 *
 */
/*
 * mysql>  select request_id, transfer_id, msg_count, endpnt from
 *  (select request_id, transfer_id, count(*) as msg_count, endpnt from msg where job_state!='ACTIVE' and job_m_replica='true' group by request_id,transfer_id) 
 *  as t1 where msg_count>1;
 */

//"file_metadata":{"adler32":"0b85a94d","name":"ESD.db97383759194f5f97fc97c58bffd319","src_rse":"IN2P3-CC_DATADISK","request_id":"3ce5e539181d49ffa8a7011c33ff0438","filesize":1048576,
//    * "dest_rse_id":"df7d9b15e89240229b860ebe0477c570","scope":"tests","dst_rse":"CA-VICTORIA-WESTGRID-T2_DATADISK","activity":"Functional Test","md5":null},
public class FTSMessage {

    private static final Log logger = LogFactory.getLog(FTSMessage.class);

    private Long cep_msg_timestamp;
    private String cep_msg_timestamp_date;
    private String cep_msg_uuid;
    
    // Add this to configuration properties
    //private static final String FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
   // private static final Locale TIMEZONE = new Locale("CH");
    
    
    private String tr_id;
    private String endpnt;
    private String src_srm_v;
    private String dest_srm_v;
    private String vo;
    private String src_url;
    private String dst_url;
    private String src_hostname;
    private String dst_hostname;
    private String src_site_name;
    private String dst_site_name;
    private String t_channel;
    private Long timestamp_tr_st;
    private String cep_timestamp_tr_st_date;
    private Long timestamp_tr_comp;
    private String cep_timestamp_tr_comp_date;
    private Long timestamp_chk_src_st;
    private String cep_timestamp_chk_src_st_date;
    private Long timestamp_chk_src_ended;
    private String cep_timestamp_chk_src_ended_date;
    private Long timestamp_checksum_dest_st;
    private String cep_timestamp_checksum_dest_st_date;
    private Long timestamp_checksum_dest_ended;
    private String cep_timestamp_checksum_dest_ended_date;
    private Long t_timeout;
    private Long chk_timeout;
    private String t_error_code;
    private String tr_error_scope;
    private String t_failure_phase;
    private String tr_error_category;
    private String t_final_transfer_state;
    private Long tr_bt_transfered;
    private Long nstreams;
    private Long buf_size;
    private Long tcp_buf_size;
    private Long block_size;
    private Long f_size;
    private Long time_srm_prep_st;
    private String cep_time_srm_prep_st;
    private Long time_srm_prep_end;
    private String cep_time_srm_prep_end;
    private Long time_srm_fin_st;
    private String cep_time_srm_fin_st;
    private Long time_srm_fin_end;
    private String cep_time_srm_fin_end;
    private String srm_space_tokeb_src;
    private String srm_space_token_dst;
    private String t__error_message;
    private Long tr_timestamp_start;
    private String cep_tr_timestamp_start_date;
    private Long tr_timestamp_complete;
    private String cep_tr_timestamp_complete_date;
    private String channel_type;
    private String user_dn;
    private FileMetadata file_metadata;
    private JobMetadata job_metadata;
    private Long retry;
    private Long retry_max;
    private String job_m_replica;
    private String job_state;
    private String file_id;

    
    private transient SimpleDateFormat dateFormat;
    
    public FTSMessage() {
        String FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
        Locale TIMEZONE = new Locale("CH");
        dateFormat = new SimpleDateFormat(FORMAT, TIMEZONE);
        this.cep_msg_timestamp = System.currentTimeMillis();
        this.cep_msg_timestamp_date = dateFormat.format(new Date(cep_msg_timestamp));
        this.cep_msg_uuid = UUID.randomUUID().toString();
    }


    /**
     * Custom Long parser To handle empty string as numerical value...
     * @author lucamag
     *
     */
    public static class LongTypeAdapter extends TypeAdapter<Long>{
        
        @Override
        public Long read(JsonReader reader) throws IOException {
            if(reader.peek() == JsonToken.NULL){
                reader.nextNull();
                return null;
            }
            String stringValue = reader.nextString();
            //Cut off subdecimal part if any
            stringValue = StringUtils.substringBefore(stringValue, ".");
            try{
                Long value = Long.valueOf(stringValue);
                return value;
            }catch(NumberFormatException e){
                //logger.info("Error "+stringValue, e);
                return null;
            }
        }
        @Override
        public void write(JsonWriter writer, Long value) throws IOException {
            if (value == null) {
                //writer.nullValue();
                writer.value("");
                return;
            }
            writer.value(value);
        }
    }

    /**
     * Builder from JSON 
     * @param raw
     * @return
     */
    public static FTSMessage fromJson(String raw) {
        //Fix for the unconsistent FTS message format...
        raw = raw.replaceAll("file_metadata\":\"\"", "file_metadata\":{}").replaceAll("job_metadata\":\"\"", "job_metadata\":{}");
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Long.class, new LongTypeAdapter());
        Gson gson = builder.create();
        FTSMessage m = gson.fromJson(raw, FTSMessage.class);
        m.setDates(); //GSON do not read methods... forcing fields set
        m.setFile_id(m.getTr_id().split("__")[3]); //Derive file ID from transfer ID
        return m;
    }
    
    /**
     * Convert to JSON
     * @return String representing the message in JSON
     */
    public String convertToJson() {
    	
        GsonBuilder builder = new GsonBuilder().setPrettyPrinting();
        builder.registerTypeAdapter(Long.class, new LongTypeAdapter());
        Gson gson = builder.create();
        return gson.toJson(this);
    }

    /**
     * Create and set human readable dates from epoch timestamps
     */
    private void setDates() {
    	if(timestamp_tr_st!=null)
    		cep_timestamp_tr_st_date = dateFormat.format(new Date(timestamp_tr_st));
    	if(timestamp_tr_comp!=null)
    		cep_timestamp_tr_comp_date = dateFormat.format(new Date(timestamp_tr_comp));
    	if(timestamp_chk_src_st!=null)
    		cep_timestamp_chk_src_st_date = dateFormat.format(new Date(timestamp_chk_src_st));
    	if(timestamp_chk_src_ended!=null)
    		cep_timestamp_chk_src_ended_date = dateFormat.format(new Date(timestamp_chk_src_ended));
    	if(timestamp_checksum_dest_st!=null)
    		cep_timestamp_checksum_dest_st_date = dateFormat.format(new Date(timestamp_checksum_dest_st));
    	if(timestamp_checksum_dest_ended!=null)
    		cep_timestamp_checksum_dest_ended_date = dateFormat.format(new Date(timestamp_checksum_dest_ended));
    	if(time_srm_prep_st!=null)
    		cep_time_srm_prep_st = dateFormat.format(new Date(time_srm_prep_st));
    	if(time_srm_prep_end!=null)
    		cep_time_srm_prep_end = dateFormat.format(new Date(time_srm_prep_end));
    	if(time_srm_fin_st!=null)
    		cep_time_srm_fin_st = dateFormat.format(new Date(time_srm_fin_st));
    	if(time_srm_fin_end!=null)
    		cep_time_srm_fin_end = dateFormat.format(new Date(time_srm_fin_end));
    	if(tr_timestamp_start!=null)
    		cep_tr_timestamp_start_date = dateFormat.format(new Date(tr_timestamp_start));
    	if(tr_timestamp_complete!=null)
    		cep_tr_timestamp_complete_date = dateFormat.format(new Date(tr_timestamp_complete));
    }
    
    public String toString() {
        return this.convertToJson();
    }
    
    
    /****************************************************************************
     * Disclaimer: read below this point at your own risk. 
     * What comes now is pure auto-generated boilerplate code for setter/getters
     * to match the message event to a Java/GSON/Esper friendly bean...
     * A justification alone for the existence of Scala.
     ****************************************************************************/
   
    
    
    /**
     * Inner classes for complex objects representation
     * @return
     */
    private class FileMetadata {
        private String adler32;
        private String name;
        private String src_rse;
        private String request_id;
        private String filesize;
        private String dest_rse_id;
        private String scope;
        private String dst_rse;
        private String activity;
        private String md5;

        public String getAdler32() {
            return adler32;
        }
        public void setAdler32(String adler32) {
            this.adler32 = adler32;
        }
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
        public String getSrc_rse() {
            return src_rse;
        }
        public void setSrc_rse(String src_rse) {
            this.src_rse = src_rse;
        }
        public String getRequest_id() {
            return request_id;
        }
        public void setRequest_id(String request_id) {
            this.request_id = request_id;
        }
        public String getFilesize() {
            return filesize;
        }
        public void setFilesize(String filesize) {
            this.filesize = filesize;
        }
        public String getDest_rse_id() {
            return dest_rse_id;
        }
        public void setDest_rse_id(String dest_rse_id) {
            this.dest_rse_id = dest_rse_id;
        }
        public String getScope() {
            return scope;
        }
        public void setScope(String scope) {
            this.scope = scope;
        }
        public String getDst_rse() {
            return dst_rse;
        }
        public void setDst_rse(String dst_rse) {
            this.dst_rse = dst_rse;
        }
        public String getActivity() {
            return activity;
        }
        public void setActivity(String activity) {
            this.activity = activity;
        }
        public String getMd5() {
            return md5;
        }
        public void setMd5(String md5) {
            this.md5 = md5;
        }
    }

    //"job_metadata":{"multi_sources":true,"issuer":"rucio"},"retry":"0","retry_max":"0","job_m_replica":"true","job_state":"FINISHED"}
    private class JobMetadata {
        private String multi_sources;
        private String issuer;
        public String getMulti_sources() {
            return multi_sources;
        }
        public void setMulti_sources(String multi_sources) {
            this.multi_sources = multi_sources;
        }
        public String getIssuer() {
            return issuer;
        }
        public void setIssuer(String issuer) {
            this.issuer = issuer;
        }

    }
    
    
    public Long getCep_msg_timestamp() {
        return cep_msg_timestamp;
    }

    public void setCep_msg_timestamp(Long msg_timestamp) {
        this.cep_msg_timestamp = msg_timestamp;
    }

    public String getCep_msg_uuid() {
        return cep_msg_uuid;
    }

    public void setCep_msg_uuid(String uuid) {
        this.cep_msg_uuid = uuid;
    }

    public void setSrc_srm_v(String src_srm) {
        this.src_srm_v = src_srm;
    }
    public String getSrc_srm_v() {
        return src_srm_v;
    }
    public String getDest_srm_v() {
        return dest_srm_v;
    }
    public void setDest_srm_v(String dest_srm) {
        this.dest_srm_v = dest_srm;
    }
    public String getSrc_hostname() {
        return src_hostname;
    }
    public void setSrc_hostname(String src_hostname) {
        this.src_hostname = src_hostname;
    }
    public String getDst_hostname() {
        return dst_hostname;
    }
    public void setDst_hostname(String dst_hostname) {
        this.dst_hostname = dst_hostname;
    }
    public String getSrc_site_name() {
        return src_site_name;
    }
    public void setSrc_site_name(String src_site_name) {
        this.src_site_name = src_site_name;
    }
    public String getDst_site_name() {
        return dst_site_name;
    }
    public void setDst_site_name(String dst_site_name) {
        this.dst_site_name = dst_site_name;
    }
    public String getT_channel() {
        return t_channel;
    }
    public void setT_channel(String t_channel) {
        this.t_channel = t_channel;
    }
    public Long getTimestamp_chk_src_st() {
        return timestamp_chk_src_st;
    }
    public void setTimestamp_chk_src_st(Long timestamp_chk_src_st) {
        this.timestamp_chk_src_st = timestamp_chk_src_st;
    }
    public Long getTimestamp_chk_src_ended() {
        return timestamp_chk_src_ended;
    }
    public void setTimestamp_chk_src_ended(Long timestamp_chk_src_ended) {
        this.timestamp_chk_src_ended = timestamp_chk_src_ended;
    }
    public Long getTimestamp_checksum_dest_st() {
        return timestamp_checksum_dest_st;
    }
    public void setTimestamp_checksum_dest_st(Long timestamp_checksum_dest_st) {
        this.timestamp_checksum_dest_st = timestamp_checksum_dest_st;
    }
    public Long getTimestamp_checksum_dest_ended() {
        return timestamp_checksum_dest_ended;
    }
    public void setTimestamp_checksum_dest_ended(Long timestamp_checksum_dest_ended) {
        this.timestamp_checksum_dest_ended = timestamp_checksum_dest_ended;
    }
    public Long getT_timeout() {
        return t_timeout;
    }
    public void setT_timeout(Long t_timeout) {
        this.t_timeout = t_timeout;
    }
    public Long getChk_timeout() {
        return chk_timeout;
    }
    public void setChk_timeout(Long chk_timeout) {
        this.chk_timeout = chk_timeout;
    }
    public String getT_error_code() {
        return t_error_code;
    }
    public void setT_error_code(String t_error_code) {
        this.t_error_code = t_error_code;
    }
    public String getTr_error_scope() {
        return tr_error_scope;
    }
    public void setTr_error_scope(String tr_error_scope) {
        this.tr_error_scope = tr_error_scope;
    }
    public String getT_failure_phase() {
        return t_failure_phase;
    }
    public void setT_failure_phase(String t_failure_phase) {
        this.t_failure_phase = t_failure_phase;
    }
    public String getTr_error_category() {
        return tr_error_category;
    }
    public void setTr_error_category(String tr_error_category) {
        this.tr_error_category = tr_error_category;
    }
    public String getT_final_transfer_state() {
        return t_final_transfer_state;
    }
    public void setT_final_transfer_state(String t_final_transfer_state) {
        this.t_final_transfer_state = t_final_transfer_state;
    }
    public Long getTr_bt_transfered() {
        return tr_bt_transfered;
    }
    public void setTr_bt_transfered(Long tr_bt_transfered) {
        this.tr_bt_transfered = tr_bt_transfered;
    }
    public Long getNstreams() {
        return nstreams;
    }


    public void setNstreams(Long nstreams) {
        this.nstreams = nstreams;
    }


    public Long getBuf_size() {
        return buf_size;
    }


    public void setBuf_size(Long buf_size) {
        this.buf_size = buf_size;
    }


    public Long getTcp_buf_size() {
        return tcp_buf_size;
    }


    public void setTcp_buf_size(Long tcp_buf_size) {
        this.tcp_buf_size = tcp_buf_size;
    }


    public Long getBlock_size() {
        return block_size;
    }


    public void setBlock_size(Long block_size) {
        this.block_size = block_size;
    }


    public Long getF_size() {
        return f_size;
    }


    public void setF_size(Long f_size) {
        this.f_size = f_size;
    }


    public Long getTime_srm_prep_st() {
        return time_srm_prep_st;
    }


    public void setTime_srm_prep_st(Long time_srm_prep_st) {
        this.time_srm_prep_st = time_srm_prep_st;
    }


    public Long getTime_srm_prep_end() {
        return time_srm_prep_end;
    }


    public void setTime_srm_prep_end(Long time_srm_prep_end) {
        this.time_srm_prep_end = time_srm_prep_end;
    }


    public Long getTime_srm_fin_st() {
        return time_srm_fin_st;
    }


    public void setTime_srm_fin_st(Long time_srm_fin_st) {
        this.time_srm_fin_st = time_srm_fin_st;
    }


    public Long getTime_srm_fin_end() {
        return time_srm_fin_end;
    }


    public void setTime_srm_fin_end(Long time_srm_fin_end) {
        this.time_srm_fin_end = time_srm_fin_end;
    }


    public String getSrm_space_tokeb_src() {
        return srm_space_tokeb_src;
    }


    public void setSrm_space_tokeb_src(String srm_space_tokeb_src) {
        this.srm_space_tokeb_src = srm_space_tokeb_src;
    }


    public String getSrm_space_token_dst() {
        return srm_space_token_dst;
    }


    public void setSrm_space_token_dst(String srm_space_token_dst) {
        this.srm_space_token_dst = srm_space_token_dst;
    }


    public String getT__error_message() {
        return t__error_message;
    }


    public void setT__error_message(String t__error_message) {
        this.t__error_message = t__error_message;
    }


    public String getChannel_type() {
        return channel_type;
    }


    public void setChannel_type(String channel_type) {
        this.channel_type = channel_type;
    }


    public String getUser_dn() {
        return user_dn;
    }


    public void setUser_dn(String user_dn) {
        this.user_dn = user_dn;
    }

    public FileMetadata getFile_metadata() {
        return file_metadata;
    }


    public void setFile_metadata(FileMetadata file_metadata) {
        this.file_metadata = file_metadata;
    }


    public JobMetadata getJob_metadata() {
        return job_metadata;
    }


    public void setJob_metadata(JobMetadata job_metadata) {
        this.job_metadata = job_metadata;
    }


    public Long getRetry() {
        return retry;
    }


    public void setRetry(Long retry) {
        this.retry = retry;
    }


    public Long getRetry_max() {
        return retry_max;
    }


    public void setRetry_max(Long retry_max) {
        this.retry_max = retry_max;
    }


    public String getJob_m_replica() {
        return job_m_replica;
    }
    public void setJob_m_replica(String job_m_replica) {
        this.job_m_replica = job_m_replica;
    }
    public String getTr_id() {
        return tr_id;
    }
    public void setTr_id(String tr_id) {
        this.tr_id = tr_id;
    }
    public String getEndpnt() {
        return endpnt;
    }
    public void setEndpnt(String endpnt) {
        this.endpnt = endpnt;
    }
    public String getVo() {
        return vo;
    }
    public void setVo(String vo) {
        this.vo = vo;
    }
    public String getSrc_url() {
        return src_url;
    }
    public void setSrc_url(String src_url) {
        this.src_url = src_url;
    }
    public String getDst_url() {
        return dst_url;
    }
    public void setDst_url(String dst_url) {
        this.dst_url = dst_url;
    }
    public Long getTimestamp_tr_st() {
        return timestamp_tr_st;
    }
    public void setTimestamp_tr_st(Long timestamp_tr_st) {
        this.timestamp_tr_st = timestamp_tr_st;
    }
    public Long getTimestamp_tr_comp() {
        return timestamp_tr_comp;
    }
    public void setTimestamp_tr_comp(Long timestamp_tr_comp) {
        this.timestamp_tr_comp = timestamp_tr_comp;
    }
    public String getFile_id() {
        return file_id;
    }
    public void setFile_id(String file_id) {
        this.file_id = file_id;
    }
    public String getJob_state() {
        return job_state;
    }
    public void setJob_state(String job_state) {
        this.job_state = job_state;
    }

    public Long getTr_timestamp_complete() {
        return tr_timestamp_complete;
    }

    public void setTr_timestamp_complete(Long tr_timestamp_complete) {
        this.tr_timestamp_complete = tr_timestamp_complete;
    }

    public Long getTr_timestamp_start() {
        return tr_timestamp_start;
    }

    public void setTr_timestamp_start(Long tr_timestamp_start) {
        this.tr_timestamp_start = tr_timestamp_start;
    }

    public String getCep_msg_timestamp_date() {
        return cep_msg_timestamp_date;
    }

    public void setCep_msg_timestamp_date(String cep_msg_timestamp_date) {
        this.cep_msg_timestamp_date = cep_msg_timestamp_date;
    }

    
    
}
