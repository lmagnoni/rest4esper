<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page session="false"%>
<html>
<head>
<title>REST 4 Esper</title>
<link href="<c:url value="/resources/css/bootstrap_w3schools.css" />" rel="stylesheet">
<link rel="stylesheet"
	href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">

<!-- Set base form Context Root -->
<c:set var="url">${pageContext.request.requestURL}</c:set>
<base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />
    

</head>
<body style="margin-left:20;margin-right:20;padding:0">
	<h1> REST API to access Esper results in real-time. </h1>

	<table class="pure-table pure-table-bordered">
		<thead>
			<tr>
				<th>ID</th>
				<th>Pattern Description</th>
				<th># Count</th>
			</tr>
		</thead>

		<tbody>
			<c:forEach var="pattern" items="${events}">
				<tr>
					<td> ${pattern.ID} </td>
					<td>${pattern.description}</td>
					<c:choose>
						<c:when test="${pattern.count=='0'}"><td>${pattern.count}</td></c:when>
						<c:otherwise> <td><a href="cep/pattern/${pattern.ID}">${pattern.count}</a></td> </c:otherwise>
					</c:choose>
				</tr>
			</c:forEach>
		</tbody>
	</table>

 <div class="footer">
  <p><p><p>
  <p>CERN/Gitlab Project: <a href="https://gitlab.cern.ch/lmagnoni/rest4esper">rest4esper</a>  Contact information: <a href="mailto:luca.magnoni@cern.ch">
  Luca Magnoni</a>.</p>
</div> 
</body>
</html>
