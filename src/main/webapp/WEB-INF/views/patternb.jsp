<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page session="false" %>
<html>
<head>
	<title>Duplicated Events</title>
	
<link href="<c:url value="/resources/css/bootstrap_w3schools.css" />" rel="stylesheet">
<link rel="stylesheet"
    href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
<!-- Set base form Context Root -->
<c:set var="url">${pageContext.request.requestURL}</c:set>
<base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />
    
	
</head>
<body style="margin-left:20;margin-right:20;padding:0">
<h1> Pattern A </h1>
<p>Detects 2 subsequent FTS messages with the same 'file_id', same 'retry' and 'endpnt' in a 10 minutes window</p>
<p/>
<div>
EPL: 
<b> 
select a,b
from pattern [every a=FTSMessage -&gt; 
b=FTSMessage(file_id=a.file_id, endpnt=a.endpnt, retry=a.retry) where timer:within(10 min)];
</b>
</div>
<p></p>
<table class="pure-table pure-table-bordered" >
<thead>
<tr>
<th>Timestamp</th>
<th>Message 1</th>
<th>Message 2</th>
<th>Transfer ID </th>
</tr>
</thead>

<tbody>
<c:forEach var="dup" items="${events}">
   <tr>
       <td>${dup.date}</td>
       <td><a href="cep/fts/message/${dup.uuid1}">${dup.uuid1}</a> </td>
       <td><a href="cep/fts/message/${dup.uuid2}">${dup.uuid2}</a> </td>
       <td>${dup.tr_id}</td>
    </tr>
</c:forEach>
</tbody>
</table>

 <div class="footer">
  <p><p><p>
  <p>CERN/Gitlab Project: <a href="https://gitlab.cern.ch/lmagnoni/rest4esper">rest4esper</a>  Contact information: <a href="mailto:luca.magnoni@cern.ch">
  Luca Magnoni</a>.</p>
</div> 

</body>
</html>
