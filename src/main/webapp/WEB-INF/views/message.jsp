<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page session="false"%>
<html>
<head>
<title>Message</title>
<link href="<c:url value="/resources/css/bootstrap_w3schools.css" />"
	rel="stylesheet">
<link rel="stylesheet"
	href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
	
<c:set var="url">${pageContext.request.requestURL}</c:set>
<base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />
 	
</head>
<body style="margin-left:20;margin-right:20;padding:0">
	<h1>Message ${message.cep_msg_uuid}</h1>
	<pre>${message.convertToJson()}</pre>

 <div class="footer">
  <p><p><p>
  <p>CERN/Gitlab Project: <a href="https://gitlab.cern.ch/lmagnoni/rest4esper">rest4esper</a>  Contact information: <a href="mailto:luca.magnoni@cern.ch">
  Luca Magnoni</a>.</p>
</div> 

</body>
</html>
