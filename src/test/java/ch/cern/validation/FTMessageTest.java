package ch.cern.validation;

import static org.junit.Assert.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.junit.Test;

import ch.cern.cep.FTSMessage;

public class FTMessageTest {
    private static final Log logger = LogFactory.getLog(FTMessageTest.class);


    @Test
    public void converstionTest() {
        String raw = "{\"tr_id\":\"2015-07-17-1005__serv02.hep.phy.cam.ac.uk__dev013-v6.gla.scotgrid.ac.uk__194330134__3ed75f34-2c6b-11e5-9736-00163e105038\",\"endpnt\":\"bnl\",\"src_srm_v\":\"2.2.0\",\"dest_srm_v\":\"2.2.0\",\"vo\":\"atlas\",\"src_url\":\"srm://serv02.hep.phy.cam.ac.uk:8446/srm/managerv2?SFN=/dpm/hep.phy.cam.ac.uk/home/atlas/atlasdatadisk/rucio/tests/f0/a4/ESD.3a3d2c3b034244de9be0eecdb53c2697\",\"dst_url\":\"srm://dev013-v6.gla.scotgrid.ac.uk:8446/srm/managerv2?SFN=/dpm/gla.scotgrid.ac.uk/home/atlas/atlasipv6test/rucio/tests/f0/a4/ESD.3a3d2c3b034244de9be0eecdb53c2697\",\"src_hostname\":\"serv02.hep.phy.cam.ac.uk\",\"dst_hostname\":\"dev013-v6.gla.scotgrid.ac.uk\",\"src_site_name\":\"\",\"dst_site_name\":\"\",\"t_channel\":\"srm://serv02.hep.phy.cam.ac.uk__srm://dev013-v6.gla.scotgrid.ac.uk\",\"timestamp_tr_st\":\"\",\"timestamp_tr_comp\":\"\",\"timestamp_chk_src_st\":\"\",\"timestamp_chk_src_ended\":\"\",\"timestamp_checksum_dest_st\":\"\",\"timestamp_checksum_dest_ended\":\"\",\"t_timeout\":\"\",\"chk_timeout\":\"1800\",\"t_error_code\":\"\",\"tr_error_scope\":\"SOURCE\",\"t_failure_phase\":\"TRANSFER_PREPARATION\",\"tr_error_category\":\"COMMUNICATION_ERROR_ON_SEND\",\"t_final_transfer_state\":\"Error\",\"tr_bt_transfered\":\"\",\"nstreams\":\"\",\"buf_size\":\"\",\"tcp_buf_size\":\"\",\"block_size\":\"0\",\"f_size\":\"\",\"time_srm_prep_st\":\"\",\"time_srm_prep_end\":\"\",\"time_srm_fin_st\":\"\",\"time_srm_fin_end\":\"\",\"srm_space_token_src\":\"\",\"srm_space_token_dst\":\"ATLASIPV6TEST\",\"t__error_message\":\"SOURCE Failed to get source file size: srm-ifce err: Communication error on send, err: [SE][Ls][] httpg://serv02.hep.phy.cam.ac.uk:8446/srm/managerv2: CGSI-gSOAP running on fts301.usatlas.bnl.gov reports could not open connection to serv02.hep.phy.cam.ac.uk:8446  \",\"tr_timestamp_start\":\"1437127510000.000000\",\"tr_timestamp_complete\":\"1437127528000.000000\",\"channel_type\":\"urlcopy\",\"user_dn\":\"/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=ddmadmin/CN=531497/CN=Robot: ATLAS Data Management\",\"file_metadata\":{\"adler32\": \"65af0190\", \"name\": \"ESD.3a3d2c3b034244de9be0eecdb53c2697\", \"src_rse\": \"IN2P3-CC_DATADISK\", \"request_id\": \"af7bcaab372043e686f8348db7bfc802\", \"filesize\": 1048576, \"dest_rse_id\": \"20e1a5ad2a2f4097aa3942170a9a6d53\", \"scope\": \"tests\", \"dst_rse\": \"UKI-SCOTGRID-GLASGOW_IPV6TEST\", \"activity\": \"User Subscriptions\", \"md5\": null},\"job_metadata\":{\"multi_sources\": true, \"issuer\": \"rucio\"},\"retry\":\"0\",\"retry_max\":\"0\",\"job_m_replica\":\"true\",\"job_state\":\"FAILED\"}";

        FTSMessage m = FTSMessage.fromJson(raw);
        assertNotNull(m);
        logger.info("json "+m.convertToJson());
        //assertEquals((Long)1437126824453Ltr_timestamp_complete, m.getTimestamp_tr_comp());
    }

}
